<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>GERENCIADOR DE TAREFAS</title>
    <link rel="stylesheet" href="style/css/style.css" />
<!-- 	 <link rel="stylesheet" href="style/css/bootstrap.css"> -->
</head>
<body >
	<!-- Os métodos do seu controller deverão manipular os elementos da sua view e tratar tais dados de acordo com sua lógica  -->
	<?php 
    include_once '../configuracao/Import.php'; // Uso da classe Import para facilitar, automatizar e deixar o código mais limpo.
    Import::controller('ControllerLogin.php'); // Antes de usar qualquer classe, é necessário incluir o arquivo referente ao mesmo.
    $controllerFormulario = new ControllerLogin(); // Instância do objeto ControllerLogin.
    $controllerFormulario->executarLogin(); // Uso do método executarLogin() para executar a lógica de manipulação e persistências dos dados digitados pelo usuario.
    ?>
    
    <!-- Formulário HTML simples -->
    <div class="login-page">
 		 <div class="form">
   			<form action="login.php" method="get" class="login-form">
     		 <input type="text" name="login" placeholder="login"/>
      		 <input type="password" name="senha" placeholder="senha"/>
      		 <button type="submit" name="submit" value="true">Acesse</button>
      			<p class="message">Já possue cadastro? <a href="./cadastro.php">Faça seu Cadastro aqui</a></p>
   		    </form>
 		 </div>
	</div>
    <script src="style/js/jquery-3.3.1.slim.min.js"></script>
    <script src="style/js/formulario.js"></script>
</body>
</html>