<?php
include_once '../configuracao/Import.php';
Import::controller('ControllerSession.php');

// Esta classe possui como finalidade fornecer o uso da 'session' em todo o escopo da classe filha do tipo Controller.
abstract class AbstractController{
    
    private $session;
    
    
    public function __construct(){
        $this->session = new Session();
        self::startSession();
    }
    
    public function startSession(){
        self::getSession()->start();
    }
    
    public function getSession(){
      return $this->session;   
    }
    
    
}


?>